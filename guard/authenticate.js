//{<Directive>
var jwt = require("jsonwebtoken");
//}</end Directive>

//{<MiddelWare>
module.exports = function (req, res, next) {
  var token =
    req.headers["token"] ||
    req.params.token ||
    req.body.token ||
    req.query.token;
  const authorizationHeader = req.headers["authorization"];
  if (authorizationHeader) token = parseBearer(authorizationHeader);

  if (!token)
    return res.status(401).send({
      success: false,
      message:
        "Failed to authenticate user due to invalid single sign-on token..!",
    });
  else {
    jwt.verify(token, "secret", (err, decoded) => {
      if (err)
        return res
          .status(500)
          .send({ success: false, message: "Your Token has been expired..!" });
      else {
        req.data = { user: decoded, token: token };
        next();
      }
    });
  }
};
//}</end MiddelWare>

const parseBearer = (bearer) => {
  const [_, token] = bearer.trim().split(" ");
  return token;
};
