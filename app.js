const express = require('express')
const app = express()

const helmet = require('helmet')
const compression = require('compression')
const cors = require('cors')

var morganBody = require('morgan-body')
const bodyParser = require('body-parser')
const routes = require('./routes/routes')

var corsOptions = {
  origin: '*',
  optionsSuccessStatus: 200
}

app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE')
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type')
  res.setHeader('Access-Control-Allow-Credentials', true)
  next()
})

app.use(helmet())
app.use(compression())
app.use(
  bodyParser.json({
    limit: '50mb'
  })
)
app.use(
  bodyParser.urlencoded({
    extended: true,
    limit: '50mb',
    parameterLimit: 50000
  })
)
//morganBody(app)
app.use(cors(corsOptions))
app.use(`/api`, routes)

app.use(function (req, res, next) {
  res.status(400).send({
    status: false,
    message: 'Request Not Found..!'
  })
  next()
})
app.use((err, req, res, next) => {
  console.log('errors')
  console.log(err)
  if (err && err.error && err.error.isJoi) {
    res.status(400).json({
      type: err.type, // will be "query" here, but could be "headers", "body", or "params"
      message: err.error.toString(),
      status: false,
    })
  } else {
    next(err)
  }
})

app.use(function (error, req, res, next) {
  if (error.errorCode) {
    res.status(error && error.errorCode ? error.errorCode.status : 500).send({
      status: false,
      message: error.message,
      desc: error.desc || ''
    })
  }
  res.status(500).send({
    status: false,
    message: 'Internal Server Error..!'
  })
})

module.exports = app
