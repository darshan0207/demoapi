const nodemailer = require("nodemailer");

var transporter = nodemailer.createTransport({
  host: process.env.HOST,
  port: 465,
  secure: true,
  auth: {
    user: process.env.SES_USERNAME,
    pass: process.env.PASSWORD,
  },
  // service: "gmail",
  // host: "smtp.gmail.com",
  // port: 25,
  // secure: true,
  // auth: {
  //   user: process.env.SENDER_ID,
  //   pass: process.env.SENDER_PASSWORD,
  // },
});

const email = (email, OTP) => {
  const text =
    "<h3>OTP for account verification is </h3>" +
    "<h1 style='font-weight:bold;'>" +
    OTP +
    "</h1>";
  var options = {
    from: '"Vibrantprintshop" <no-reply@vibrantprintshop.com>',
    to: email,
    subject: "OTP for registration",
    html: text,
  };
  return sendEmail(options)
    .then(() => {
      return "OTP sent to your email successful..! Please check your email.";
    })
    .catch((error) => {
      throw new Error(error);
    });
};

const forgetPasswordEmail = (email, link) => {
  const text =
    "<h3>Forgot your password?</h3>" +
    "<p>No problem—we'll help you out! To reset your password, simply click the button below.</p>" +
    "<a target='_blank' href=" +
    link +
    " style='font-weight:bold;'>reset password</a>";
  var options = {
    from: '"Vibrantprintshop" <no-reply@vibrantprintshop.com>',
    to: email,
    subject: "Forgot your password?",
    html: text,
  };
  return sendEmail(options)
    .then(() => {
      return "Password reset email has been sent to your email address if it is valid email address.";
    })
    .catch((error) => {
      throw new Error(error);
    });
};

const sendEmail = (options) => {
  return new Promise((resolve, reject) => {
    transporter.sendMail(options, (error, info) => {
      if (error) return reject(error);
      else
        return resolve({
          status: true,
        });
    });
  });
};

module.exports = {
  email: email,
  forgetPasswordEmail: forgetPasswordEmail,
};
