const mongoose = require('mongoose')
require('dotenv').config()

const connect = () => {
  return new Promise((resolve, reject) => {
    mongoose
      .connect(process.env.MONGOPATH, {
        useNewUrlParser: true,
        useUnifiedTopology: true
      })
      .then(() => {
        console.log('DB connection successful!')
        resolve()
      })
      .catch(error => {
        console.log('Database connection failed')
        console.error(error)
        reject(error)
      })
  })
}
const disconnect = () => {
  mongoose.disconnect()
}

module.exports = {
  connect: connect,
  disconnect: disconnect
}
