var mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");

var shippingCategory = new mongoose.Schema({
  name: { type: String, required: true },
});

shippingCategory.plugin(mongoosePaginate);

var shippingCategorys = mongoose.model("shippingCategory", shippingCategory);

module.exports = {
  shippingCategory: shippingCategorys,
};
