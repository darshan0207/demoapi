var schema = require("./schema");

module.exports = class shippingCategoryService {
  async list(req) {
    try {
      let page = parseInt(req.query.page) || 1;
      let limit = parseInt(req.query.limit) || 30;

      var query = {};
      var options = {
        limit: limit,
        page: page,
        lean: true,
      };
      return await schema.shippingCategory.paginate(query, options);
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }
  get(id) {
    try {
      return schema.shippingCategory.findOne({ _id: id });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  create(req) {
    try {
      return schema.shippingCategory
        .findOne({
          name: req.body.name,
        })
        .then(async (data) => {
          if (data && data.data) {
            throw new Error("payment method already exist");
          } else {
            const payload = new schema.shippingCategory(req.body);
            return await payload.save();
          }
        });
    } catch (err) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  delete(id) {
    try {
      return schema.shippingCategory.deleteOne({ _id: id });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  edit(id, data) {
    try {
      return schema.shippingCategory.findOneAndUpdate(
        { _id: id },
        { $set: data },
        { upsert: false, new: true }
      );
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }
};
