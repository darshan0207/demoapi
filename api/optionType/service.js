var schema = require("./schema");

module.exports = class OptionTypeService {
  list(req) {
    try {
      return schema.optionType.find();
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  get(id) {
    try {
      return schema.optionType.findOne({ _id: id });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  create(req) {
    try {
      return schema.optionType
        .findOne({
          name: req.body.name,
        })
        .then(async (optionType) => {
          if (optionType && optionType.name) {
            throw new Error("Option type already exist");
          } else {
            const data = new schema.optionType(req.body);
            return await data.save();
          }
        });
    } catch (err) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  delete(id) {
    try {
      return schema.optionType.deleteOne({ _id: id });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  edit(id, data) {
    try {
      return schema.optionType.findOneAndUpdate(
        { _id: id },
        { $set: data },
        { upsert: false, new: true }
      );
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }
};
