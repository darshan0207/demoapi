var mongoose = require("mongoose");
var optionType = new mongoose.Schema({
  name: { type: String, required: true },
  presentation: { type: String },
  description: { type: String },
  min: { type: Number },
  max: { type: Number },
  required: { type: Boolean, default: true },
  filterable: { type: Boolean, default: true },
  sort: { type: Number, default: 0 },
  allow_quantity: { type: Boolean, default: false },
});

var optionTypes = mongoose.model("optiontypes", optionType);

module.exports = {
  optionType: optionTypes,
};
