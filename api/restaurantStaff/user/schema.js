const mongoose = require("mongoose");
const argon2 = require("argon2");
const mongoosePaginate = require("mongoose-paginate-v2");

const restaurantPerson = new mongoose.Schema(
  {
    firstName: {
      type: String,
      required: true,
    },
    lastName: {
      type: String,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      unique: true,
    },
    phone: {
      type: String,
      trim: true,
    },
    phoneOtp: { type: String },
    role: {
      type: String,
      default: "restaurant",
    },

    token: { type: String },
    editable: {
      type: Boolean,
      default: false,
    },
    active: {
      type: Boolean,
      default: false,
    },
    otp: {
      type: String,
    },
  },
  { timestamps: true }
);

restaurantPerson.plugin(mongoosePaginate);

restaurantPerson.pre("save", async function (next) {
  if (this.isModified("password")) {
    this.password = await argon2.hash(this.password);
  }
  next();
});

var restaurantPersons = mongoose.model("restaurant-users", restaurantPerson);

module.exports = {
  restaurantPerson: restaurantPersons,
};
