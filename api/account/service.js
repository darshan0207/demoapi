var schema = require("../auth/schema");
const moment = require("moment");
const argon2 = require("argon2");
const emailvalidator = require("email-validator");

module.exports = class AccountService {
  list = async (req) => {
    try {
      let page = parseInt(req.query.page) || 1;
      let limit = parseInt(req.query.limit) || 30;
      var query = {};
      var options = {
        select: "-password",
        limit: limit,
        page: page,
        lean: true,
      };
      return await schema.signup.paginate(query, options);
    } catch (error) {
      throw new Error(error);
    }
  };

  get(id) {
    try {
      return schema.signup.findOne({ _id: id }, { password: 0 });
    } catch (error) {
      throw new Error(error);
    }
  }

  async create(req) {
    try {
      const { firstName, lastName, email, password, role, active } = req.body;
      if (!email) throw new Error("Please Enter Email");
      if (!password) throw new Error("Please Enter password");
      if (!firstName) throw new Error("Please Enter First Name");
      if (!lastName) throw new Error("Please Enter Last Name");

      if (!emailvalidator.validate(email)) {
        throw new Error("Invalid Email");
      }
      const userEmail = await schema.signup.findOne({ email: email });
      if (userEmail) {
        throw new Error("Email already Exist");
      }
      const signup = new schema.signup({
        firstName: firstName,
        lastName: lastName,
        password: password,
        email: email,
        role: role,
        active: active,
      });
      return await signup.save();
    } catch (error) {
      throw new Error(error);
    }
  }

  delete(id) {
    try {
      return schema.signup.deleteOne({ _id: id });
    } catch (error) {
      throw new Error(error);
    }
  }

  edit = async (id, req) => {
    try {
      if (req.body.password) {
        req.body.password = await argon2.hash(req.body.password);
      }
      return await schema.signup.findOneAndUpdate(
        { _id: id },
        { $set: req.body },
        { upsert: false, new: true, select: { password: 0 } }
      );
    } catch (error) {
      throw new Error(error);
    }
  };

  getUserFilterData = async (req) => {
    try {
      const startDate = moment(req.query.startDate);
      startDate.set(0, 0, 0, 0);
      const endDate = moment(req.query.endDate);
      endDate.set({ hour: 23, minute: 59, second: 59, millisecond: 999 });
      return await schema.signup.countDocuments({
        createdAt: {
          $gte: startDate,
          $lt: endDate,
        },
      });
    } catch (error) {
      throw new Error(error);
    }
  };
};
