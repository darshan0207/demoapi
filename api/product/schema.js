var mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");
var product = new mongoose.Schema({
  name: { type: String, required: true },
  slug: { type: String },
  description: { type: String },
  price: { type: String },
  compareatprice: { type: String },
  costprice: { type: String },
  costcurrency: { type: String },
  availableon: { type: String },
  discontinueon: { type: String },
  promotionable: { type: Boolean },
  returnable: { type: Boolean },
  returndays: { type: String },
  shippingcategory: { type: String },
  taxcategory: { type: String },
  taxrate: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "taxrates",
  },
  taxon: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "taxons",
    },
  ],
  optiontype: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "optiontypes",
    },
  ],
  metatitle: { type: String },
  metakeywords: { type: String },
  metadescription: { type: String },
  images: [
    {
      path: { type: String },
      alttext: { type: String },
      variantid: { type: String },
    },
  ],
  variants: [
    {
      optionvalueids: [
        {
          type: mongoose.Schema.Types.ObjectId,
          ref: "optionvalues",
        },
      ],
      sku: { type: String },
      price: { type: String },
      compareatprice: { type: String },
      costprice: { type: String },
      taxcategoryid: { type: String },
      discontinueon: { type: String },
      weight: { type: String },
      height: { type: String },
      width: { type: String },
      depth: { type: String },
    },
  ],
  propertys: [
    {
      name: { type: String },
      value: { type: String },
      showproperty: { type: Boolean },
    },
  ],
  stock: [
    {
      quantity: { type: String },
      locationid: { type: String },
      variantid: { type: String },
    },
  ],
  relatedproduct: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "products",
    },
  ],
  categorie_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "categories",
  },
  subcategorie_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "subcategories",
  },
  suggested: [
    {
      title: String,
      description: String,
      subdescription: String,
      required: Boolean,
      product: [
        {
          type: mongoose.Schema.Types.ObjectId,
          ref: "products",
        },
      ],
      multiple: Boolean,
      maximum: Number,
    },
  ],
  groups: [
    {
      group_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "optiontypes",
      },
      options: [
        {
          type: mongoose.Schema.Types.ObjectId,
          ref: "optionvalues",
        },
      ],
    },
  ],
  tags: [
    {
      type: String,
    },
  ],
  active: { type: Boolean, default: true },
  hidden_until: { type: String },
  active_begin: { type: Number },
  active_days: { type: Number, default: 0 },
  active_end: { type: Number },
  extras: { type: String },
  hidden_until: { type: String },
  hide_instructions: { type: Boolean, default: false },
  is_out_of_stock: { type: Boolean, default: false },
  out_of_stock: { type: Boolean, default: false },
  out_of_stock_next_day: { type: String },
  out_of_stock_until: { type: String },
});
product.plugin(mongoosePaginate);

var products = mongoose.model("products", product);

module.exports = {
  product: products,
};
