var mongoose = require("mongoose");
var schema = require("./schema");
const OptionTypeService = require("../optionType/service");
const OptionValueService = require("../optionValue/service");
const categorieService = require("../categorie/service");
const subCategorieService = require("../subcategorie/service");
module.exports = class productService {
  constructor() {
    this.optionTypeService = new OptionTypeService();
    this.optionValueService = new OptionValueService();
    this.categorieService = new categorieService();
    this.subCategorieService = new subCategorieService();
  }

  async list(req) {
    try {
      var query = {},
        filter = {};

      query["$in"] = [];
      if (req.query.color) {
        query["$in"].push(mongoose.Types.ObjectId(req.query.color));
      }
      if (req.query.length) {
        query["$in"].push(mongoose.Types.ObjectId(req.query.length));
      }
      if (req.query.size) {
        query["$in"].push(mongoose.Types.ObjectId(req.query.size));
      }
      if (query["$in"].length) {
        filter["variants.optionvalueids"] = query;
      }

      if (req.query.name) {
        let querylike = new RegExp(req.query.name, "i");
        filter["$or"] = [];
        filter["$or"].push(
          {
            name: querylike,
          },
          {
            metatitle: querylike,
          },
          {
            metakeywords: querylike,
          }
        );
      }
      if (req.query.admin) {
        let page = parseInt(req.query.page) || 1;
        let limit = parseInt(req.query.limit) || 30;

        var options = {
          limit: limit,
          page: page,
          lean: true,
        };
        return await schema.product.paginate(filter, options);
      } else {
        return await schema.product.find(filter);
      }
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  get(slug) {
    try {
      return schema.product
        .findOne({ slug: slug })
        .populate([
          "optiontype",
          "taxon",
          "variants.optionvalueids",
          "relatedproduct",
          "suggested.product",
          "taxrate",
          "groups.group_id",
          "groups.options",
        ])
        .populate({
          path: "relatedproduct",
          populate: { path: "categorie_id" },
        });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  async getoptionvalue(payload) {
    try {
      const result = await this.optionValueService.findAll(payload._id);
      return {
        _id: payload._id,
        name: payload.name,
        presentation: payload.presentation,
        filterable: payload.filterable,
        optionvalues: result,
      };
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  async getfilters() {
    try {
      const promises = [];
      const result = await this.optionTypeService.list();
      for (const data of result) {
        promises.push(this.getoptionvalue(data));
      }
      return Promise.all(promises).then((values) => {
        return values;
      });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  create(req) {
    try {
      return schema.product
        .findOne({
          name: req.body.name,
        })
        .then(async (product) => {
          if (product && product.name) {
            throw new Error("product already exist");
          } else {
            const data = new schema.product(req.body);
            return await data.save();
          }
        });
    } catch (err) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  delete(id) {
    try {
      return schema.product.deleteOne({ _id: id });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  edit(id, data) {
    try {
      return schema.product.findOneAndUpdate(
        { _id: id },
        { $set: data },
        { upsert: false, new: true }
      );
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }
  async productList(req) {
    try {
      if (req.session && req.session.products) {
        console.log("this is from cart");
        return req.session.products;
      } else {
        return await this.createSession(req);
      }

      // return categories
    } catch (error) {
      console.log(error);
      throw new Error("The resource you were looking for could not be found");
    }
  }
  async clearSession(req) {
    if (req.session) delete req.session.products;
    await this.createSession(req);
  }
  async createSession(req) {
    const categories = await this.categorieService.list();
    if (categories && categories.length) {
      const promises = [];
      for (var i = 0; i < categories.length; i++) {
        promises.push(this.getSubCategories(categories[i]));
      }
      return Promise.all(promises).then(function (values) {
        // console.log(values);
        if (req.session) req.session.products = values;
        return values;
      });
    }
  }
  async getSubCategories(categorie) {
    return new Promise(async (resolve, reject) => {
      var subCategories = await this.subCategorieService.getByCategory(
        categorie._id
      );
      categorie["subCategories"] = subCategories;
      if (subCategories && subCategories.length) {
        for (let index = 0; index < subCategories.length; index++) {
          const products = await this.getProducts({
            subcategorie_id: subCategories[index]._id,
          });
          categorie["subCategories"][index]["products"] = products;
        }
      } else {
        const products = await this.getProducts({
          categorie_id: categorie._id,
        });
        categorie["products"] = products;
      }
      return resolve(categorie);
      // Promise.all([
      //   getData('SchemaMaster.FinishType', 'finishtype', entityName, id)
      // ]).then(function (values) {
      //   // console.log(values);
      //   return resolve({
      //     data: values,
      //     entityName: entityName
      //   })
      // })
    });
  }
  async getProducts(params) {
    return schema.product.find(params).lean();
  }
};
