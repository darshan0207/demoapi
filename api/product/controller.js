const express = require("express");
const router = express.Router();
const Service = require("./service");
const service = new Service();

router.get("/productlist", (req, res) => {
  service
    .productList(req)
    .then((data) => res.send({ status: true, data }))
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});
router.get("/", (req, res) => {
  service
    .list(req)
    .then((data) => res.send({ status: true, data }))
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});

router.get("/filters", (req, res) => {
  service
    .getfilters()
    .then((data) => res.send({ status: true, data }))
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});

router.get("/:slug", (req, res) => {
  service
    .get(req.params.slug)
    .then((data) => res.send({ status: true, data }))
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});

router.post("/", (req, res) => {
  service
    .create(req)
    .then((data) => {
      service.clearSession(req);
      res.status(200).send({ status: true, data });
    })
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});

router.delete("/:id", (req, res) => {
  service
    .delete(req.params.id)
    .then((data) => {
      service.clearSession(req);
      res.send({ status: true, data });
    })
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});

router.put("/:id", (req, res) => {
  service
    .edit(req.params.id, req.body)
    .then((data) => {
      service.clearSession(req);
      res.send({ status: true, data });
    })
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});

module.exports = router;
