var mongoose = require("mongoose");

var pickupAddress = new mongoose.Schema({
  _id: { type: Number, default: 1 },
  address: { type: String, required: true },
  city: { type: String, required: true },
  country: { type: String, required: true },
  latitude: { type: String, required: true },
  longitude: { type: String, required: true },
  postalcode: { type: String, required: true },
  region: { type: String, required: true },
  streetaddress: { type: String, required: true },
  locationtype: { type: String, default: "PHYSICAL" },
});

var pickupaddress = mongoose.model("pickupaddress", pickupAddress);

module.exports = {
  pickupaddress: pickupaddress,
};
