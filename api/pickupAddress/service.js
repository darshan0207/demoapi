var schema = require("./schema");

module.exports = class PickupAddressService {
  async list(req) {
    try {
      return await schema.pickupaddress.find();
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  get(id) {
    try {
      return schema.pickupaddress.findOne({ _id: id });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  edit(id, data) {
    try {
      return schema.pickupaddress.findOneAndUpdate(
        { _id: id },
        { $set: data },
        { upsert: true, new: true }
      );
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }
};
