var schema = require("./schema");

module.exports = class taxonService {
  list(req) {
    try {
      return schema.taxon.find();
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  get(id) {
    try {
      return schema.taxon.findOne({ _id: id });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  getByTaxonomyId(id) {
    try {
      return schema.taxon.find({ taxonomy_id: id });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  create(req) {
    try {
      return schema.taxon
        .findOne({
          name: req.body.name,
          parent_id: req.body.name,
        })
        .then(async (taxon) => {
          if (taxon && taxon.name) {
            throw new Error("taxon already exist");
          } else {
            const data = new schema.taxon(req.body);
            return await data.save();
          }
        });
    } catch (err) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  delete(id) {
    try {
      return schema.taxon.deleteOne({ _id: id });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  edit(id, data) {
    try {
      return schema.taxon.findOneAndUpdate(
        { _id: id },
        { $set: data },
        { upsert: false, new: true }
      );
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }
};
