var mongoose = require("mongoose");
var taxon = new mongoose.Schema({
  taxonomy_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "taxonomies",
  },
  parent_id: { type: String },
  name: { type: String, required: true },
  position: { type: Number, default: 0 },
  permalink: { type: String },
  lft: { type: Number, default: 0 },
  rgt: { type: Number, default: 0 },
  description: { type: String },
  meta_title: { type: String },
  meta_description: { type: String },
  meta_keywords: { type: String },
  depth: { type: Number, default: 0 },
  pretty_name: { type: String },
  seo_title: { type: String },
  image: { type: String },
  is_root: { type: Boolean, default: false },
  is_child: { type: Boolean, default: false },
  is_leaf: { type: Boolean, default: false },
});

var taxons = mongoose.model("taxons", taxon);

module.exports = {
  taxon: taxons,
};
