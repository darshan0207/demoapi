var schema = require("./schema");

module.exports = class countryService {
  async list(req) {
    try {
      let page = parseInt(req.query.page) || 1;
      let limit = parseInt(req.query.limit) || 30;
      var query = {};
      var options = {
        limit: limit,
        page: page,
        lean: true,
      };
      return await schema.country.paginate(query, options);
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  get(id) {
    try {
      return schema.country.findOne({ _id: id });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  create(req) {
    try {
      return schema.country
        .findOne({
          name: req.body.name,
        })
        .then(async (data) => {
          if (data && data.name) {
            throw new Error("country already exist");
          } else {
            const data = new schema.country(req.body);
            return await data.save();
          }
        });
    } catch (err) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  edit(id, data) {
    try {
      return schema.country.findOneAndUpdate(
        { _id: id },
        { $set: data },
        { upsert: false, new: true }
      );
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  delete(id) {
    try {
      return schema.country.deleteOne({ _id: id });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }
};
