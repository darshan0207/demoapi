var mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");

var country = new mongoose.Schema({
  name: { type: String, required: true },
  isoname: { type: String },
  iso: { type: String, required: true },
  iso3: { type: String },
  statesrequired: { type: Boolean, default: false },
  zipcoderequired: { type: Boolean, default: true },
});
country.plugin(mongoosePaginate);

var countrys = mongoose.model("countrys", country);

module.exports = {
  country: countrys,
};
