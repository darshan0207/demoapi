var mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");

var categorie = new mongoose.Schema({
  name: { type: String, required: true },
  slug: { type: String, required: true },
  position: { type: Number, default: 0 },
  description: { type: String },
  image: { type: String },
  active: { type: Boolean, default: true },
  extras: { type: String },
  hidden_until: { type: String },
  active_begin: { type: Number },
  active_days: { type: Number, default: 0 },
  active_end: { type: Number },
  seo_title: { type: String },
  meta_title: { type: String },
  meta_description: { type: String },
  meta_keywords: { type: String },
});

categorie.plugin(mongoosePaginate);

var categories = mongoose.model("categories", categorie);

module.exports = {
  categorie: categories,
};
