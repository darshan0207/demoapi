const express = require("express");
const router = express.Router();
const Service = require("./service");
const service = new Service();
const ProductService = require("../product/service");
const productService = new ProductService();
router.get("/", (req, res) => {
  service
    .list(req)
    .then((data) => res.send({ status: true, data }))
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});

router.get("/:id", (req, res) => {
  service
    .get(req.params.id)
    .then((data) => res.send({ status: true, data }))
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});

router.post("/", (req, res) => {
  service
    .create(req)
    .then((data) => {
      productService.clearSession(req);
      res.status(200).send({ status: true, data });
    })
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});

router.delete("/:id", (req, res) => {
  service
    .delete(req.params.id)
    .then((data) => {
      productService.clearSession(req);
      res.send({ status: true, data });
    })
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});

router.put("/:id", (req, res) => {
  service
    .edit(req.params.id, req.body)
    .then((data) => {
      productService.clearSession(req);
      res.send({ status: true, data });
    })
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});

module.exports = router;
