var schema = require("./schema");

module.exports = class categorieService {
  async list(req) {
    try {
      var query = {};
      if (req && req.query.admin) {
        let page = parseInt(req.query.page) || 1;
        let limit = parseInt(req.query.limit) || 30;
        var options = {
          limit: limit,
          page: page,
          lean: true,
        };
        return await schema.categorie.paginate(query, options);
      } else {
        query["active"] = true;
        return await schema.categorie.find(query).lean();
      }
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  get(id) {
    try {
      return schema.categorie.findOne({ _id: id });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  create(req) {
    try {
      return schema.categorie
        .findOne({
          name: req.body.name,
        })
        .then(async (categorie) => {
          if (categorie && categorie.name) {
            throw new Error("Categorie already exist");
          } else {
            const data = new schema.categorie(req.body);
            return await data.save();
          }
        });
    } catch (err) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  delete(id) {
    try {
      return schema.categorie.deleteOne({ _id: id });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  edit(id, data) {
    try {
      return schema.categorie.findOneAndUpdate(
        { _id: id },
        { $set: data },
        { upsert: false, new: true }
      );
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }
};
