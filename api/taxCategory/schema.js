var mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");

var taxcategory = new mongoose.Schema({
  _id: { type: Number, default: 1 },
  label: { type: String, required: true },
  type: { type: String, enum: ["GROSS", "NET"], default: "NET" },
  convenience: { type: String },
  delivery: { type: String },

  // name: { type: String, required: true },
  // description: { type: String },
  // isdefault: { type: Boolean, default: false },
  // taxcode: {
  //   type: String,
  // },
});
taxcategory.plugin(mongoosePaginate);

var taxcategorys = mongoose.model("taxcategorys", taxcategory);

module.exports = {
  taxcategory: taxcategorys,
};
