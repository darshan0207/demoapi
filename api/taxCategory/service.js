var schema = require("./schema");

module.exports = class taxcategoryService {
  async list(req) {
    try {
      // if (req.query.admin) {
      //   let page = parseInt(req.query.page) || 1;
      //   let limit = parseInt(req.query.limit) || 30;
      //   var query = {};
      //   var options = {
      //     limit: limit,
      //     page: page,
      //     lean: true,
      //   };
      //   return await schema.taxcategory.paginate(query, options);
      // } else {
      return await schema.taxcategory.find({ _id: id });
      // }
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  get(id) {
    try {
      return schema.taxcategory.findOne({ _id: id });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  create(req) {
    try {
      return schema.taxcategory
        .findOne({
          label: req.body.label,
        })
        .then(async (data) => {
          if (data && data.label) {
            throw new Error("tax category already exist");
          } else {
            const data = new schema.taxcategory(req.body);
            return await data.save();
          }
        });
    } catch (err) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  edit(id, data) {
    try {
      return schema.taxcategory.findOneAndUpdate(
        { _id: id },
        { $set: data },
        { upsert: false, new: true }
      );
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  delete(id) {
    try {
      return schema.taxcategory.deleteOne({ _id: id });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }
};

const defaultTaxCategory = async () => {
  return await schema.taxcategory.updateOne(
    { _id: 1 },
    {
      $set: {
        label: "PVM",
        type: "NET",
        convenience: "0",
        delivery: "0",
      },
    },
    { upsert: true }
  );
};
defaultTaxCategory();
