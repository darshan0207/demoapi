var mongoose = require("mongoose");
var propertieType = new mongoose.Schema({
  name: { type: String, required: true },
  presentation: { type: String, required: true },
  filterable: { type: Boolean, default: true },
});

var propertieTypes = mongoose.model("properties", propertieType);

module.exports = {
  propertieType: propertieTypes,
};
