var schema = require("./schema");

module.exports = class propertieTypeService {
  list(req) {
    try {
      return schema.propertieType.find();
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  get(id) {
    try {
      return schema.propertieType.findOne({ _id: id });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  create(req) {
    try {
      return schema.propertieType
        .findOne({
          name: req.body.name,
        })
        .then(async (propertieType) => {
          if (propertieType && propertieType.name) {
            throw new Error("Propertie already exist");
          } else {
            const data = new schema.propertieType(req.body);
            return await data.save();
          }
        });
    } catch (err) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  delete(id) {
    try {
      return schema.propertieType.deleteOne({ _id: id });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  edit(id, data) {
    try {
      return schema.propertieType.findOneAndUpdate(
        { _id: id },
        { $set: data },
        { upsert: false, new: true }
      );
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }
};
