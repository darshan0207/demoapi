var mongoose = require("mongoose");
var taxonomieType = new mongoose.Schema({
  name: { type: String, required: true },
});

var taxonomieTypes = mongoose.model("taxonomies", taxonomieType);

module.exports = {
  taxonomieType: taxonomieTypes,
};
