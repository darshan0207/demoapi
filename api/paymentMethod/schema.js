var mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");

var paymentMethod = new mongoose.Schema({
  name: { type: String, required: true },
  type: { type: String },
  description: { type: String },
  active: { type: Boolean, default: false },
  autocapture: { type: Boolean, default: true },
  displayon: {
    type: String,
    enum: ["both", "backend", "frontend"],
    default: "both",
  },
});

paymentMethod.plugin(mongoosePaginate);

var paymentMethods = mongoose.model("paymentMethod", paymentMethod);

module.exports = {
  paymentMethod: paymentMethods,
};
