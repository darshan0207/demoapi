const express = require("express");
const router = express.Router();
const Service = require("./service");
const service = new Service();

router.get("/", (req, res) => {
  service
    .list(req)
    .then((data) =>
      res.send({
        status: true,
        data,
      })
    )
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});

router.get("/:id", (req, res) => {
  service
    .get(req.params.id)
    .then((data) => res.send({ status: true, data }))
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});

router.post("/", (req, res) => {
  service
    .create(req)
    .then((data) => res.status(200).send({ status: true, data }))
    .catch((err) =>
      res.status(400).send({
        status: false,
        message: err.message,
      })
    );
});

router.post("/associate", (req, res) => {
  service
    .associate(req)
    .then((data) => res.send({ status: true, data }))
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});

router.put("/:id", (req, res) => {
  service
    .edit(req.params.id, req.body)
    .then((data) => res.send({ status: true, data }))
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});

router.put("/updateitem/:id", (req, res) => {
  service
    .updateitem(req.params.id, req.body)
    .then((data) => res.send({ status: true, data }))
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});

router.put("/setquantity/:id", (req, res) => {
  service
    .setquantity(req.params.id, req)
    .then((data) => res.send({ status: true, data }))
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});

router.delete("/:id", (req, res) => {
  service
    .delete(req.params.id)
    .then((data) => res.send({ status: true, data }))
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});

router.delete("/deleteMany/:id", (req, res) => {
  service
    .deleteMany(req.params.id)
    .then((data) => res.send({ status: true, data }))
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});

router.delete("/deleteitem/:id", (req, res) => {
  service
    .deleteitem(req)
    .then((data) => res.send({ status: true, data }))
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});

module.exports = router;
