var mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");

var cart = new mongoose.Schema({
  // orderId: { type: String },
  email: { type: String },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "signups",
    required: true,
  },
  productId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "products",
    required: true,
  },
  suggested: [mongoose.Schema.Types.Mixed],
  groups: [mongoose.Schema.Types.Mixed],
  // variantId: {
  //   type: String,
  //   required: true,
  // },
  quantity: { type: Number, min: 1, required: true },
  price: { type: Number, min: 1, required: true },
  variantprice: { type: Number, default: 0 },
});
cart.plugin(mongoosePaginate);

var carts = mongoose.model("carts", cart);

module.exports = {
  cart: carts,
};
