var schema = require("./schema");
var jwt = require("jsonwebtoken");
const { ObjectId } = require("mongodb");

module.exports = class cartService {
  async list(req) {
    try {
      let page = parseInt(req.query.page) || 1;
      let limit = parseInt(req.query.limit) || 30;
      var query = {};
      if (req.query.user) query["user"] = req.query.user;
      return await schema.cart
        .find(query)
        .populate(["productId"])
        .populate({
          path: "productId",
          populate: {
            path: "taxrate",
            // populate: {
            //   path: "taxcategory",
            // },
          },
        })
        .lean();
      // return await this.cartsummary(this.filterby(req));
      // return schema.cart.find(filter).populate(["productId"]);
      // var myAggregate = schema.cart.aggregate();
      // const result = await schema.cart.aggregatePaginate(myAggregate, {
      //   page,
      //   limit,
      // });
      // return result;
    } catch (error) {
      throw new Error(error);
    }
  }

  get(id) {
    try {
      return schema.cart.findOne({ _id: id }).populate(["productId"]);
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  async create(req) {
    try {
      // const { quantity } = req.body;
      // await schema.cart.findOneAndUpdate(
      //   { productId: findbyfilter.productId },
      //   { $set: req.body, $inc: { quantity: quantity } },
      //   { upsert: true, new: true }
      // );
      // delete findbyfilter["productId"];
      // delete findbyfilter["variantId"];
      // return await this.cartsummary(findbyfilter);
      const data = new schema.cart(req.body);
      return await data.save();
    } catch (err) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  // async create(req) {
  //   try {
  //     let findbyfilter = await this.filterby(req);
  //     if (req.data.user.orderId) {
  //       req.body["orderId"] = req.data.user.orderId;
  //     } else {
  //       req.body["email"] = req.data.user.email;
  //     }
  //     findbyfilter["productId"] = req.body.productId;
  //     findbyfilter["variantId"] = req.body.variantId;
  //     const { quantity } = req.body;
  //     delete req.body.quantity;
  //     await schema.cart.findOneAndUpdate(
  //       findbyfilter.variantId
  //         ? { variantId: findbyfilter.variantId }
  //         : { productId: findbyfilter.productId },
  //       { $set: req.body, $inc: { quantity: quantity } },
  //       { upsert: true, new: true }
  //     );
  //     delete findbyfilter["productId"];
  //     delete findbyfilter["variantId"];
  //     return await this.cartsummary(findbyfilter);
  //   } catch (err) {
  //     throw new Error("The resource you were looking for could not be found");
  //   }
  // }

  edit(id, data) {
    try {
      return schema.cart
        .findOneAndUpdate(
          { _id: id },
          { $set: data },
          { upsert: false, new: true }
        )
        .populate(["productId"]);
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  delete(id) {
    try {
      return schema.cart.deleteOne({ _id: id });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  deleteMany(id) {
    try {
      return schema.cart.deleteMany({ user: id });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  updateitem(id, data) {
    try {
      return schema.cart.updateMany(
        { orderId: id },
        { $set: data },
        { upsert: false, new: true }
      );
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  async setquantity(id, req) {
    try {
      await schema.cart.findOneAndUpdate(
        { _id: id },
        { $set: { quantity: req.body.quantity } },
        { upsert: false, new: true }
      );
      return await this.cartsummary(this.filterby(req));
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  async deleteitem(req) {
    try {
      await schema.cart.deleteOne({ _id: req.params.id });
      return await this.cartsummary(this.filterby(req));
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  async associate(req) {
    try {
      let token = await jwt.verify(req.body.ordertoken, "secret");
      if (!token && (!token.orderId || !token.email)) {
        throw new Error("The resource you were looking for could not be found");
      }
      let filter = {};
      if (token.email) {
        filter["email"] = token.email;
      } else {
        filter["orderId"] = token.orderId;
      }
      await schema.cart.deleteMany({ email: req.data.user.email });

      return schema.cart.updateMany(
        filter,
        { $set: { email: req.data.user.email } },
        { upsert: false, new: true }
      );
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  async cartsummary(req) {
    try {
      let cart = await schema.cart.find(req).populate(["productId"]).lean();
      if (cart && cart.length) {
        let itemtotal = 0;
        let cartitem = cart.map((data) => {
          if (data.variantId) {
            let priceN = data.productId.variants.find(
              (ele) => ObjectId(ele._id).toString() === data.variantId
            );
            data["newprice"] = priceN.price;
            itemtotal += data.quantity * data.newprice;
            data["total"] = data.quantity * data.newprice;
            return data;
          } else {
            itemtotal += data.quantity * data.productId.price;
            data["total"] = data.quantity * data.productId.price;
            return data;
          }
        });
        return {
          data: cartitem,
          itemtotal: itemtotal,
        };
      }
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  filterby(req) {
    let filter = {};
    req.data.user.orderId
      ? (filter["orderId"] = req.data.user.orderId)
      : (filter["email"] = req.data.user.email);
    return filter;
  }
};
