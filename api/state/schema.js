var mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");

var state = new mongoose.Schema({
  name: { type: String, required: true },
  iso: { type: String, required: true },
  country: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "countrys",
    required: true,
  },
});
state.plugin(mongoosePaginate);

var states = mongoose.model("states", state);

module.exports = {
  state: states,
};
