const express = require("express");
const router = express.Router();
const Service = require("./service");
const service = new Service();

router.post("/login", (req, res) => {
  service
    .login(req)
    .then((data) => res.status(200).send({ status: true, result: data }))
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});

router.post("/signup", (req, res) => {
  service
    .signup(req)
    .then((data) => res.send({ status: true, data }))
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});

router.post("/forgetpassword", (req, res) => {
  service
    .forgetPassword(req)
    .then((data) => res.send({ status: true, data }))
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});

router.post("/resetpassword", (req, res) => {
  service
    .resetPassword(req)
    .then((data) => res.send({ status: true, data }))
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});

router.post("/refreshtoken", (req, res) => {
  service
    .refreshToken(req)
    .then((data) => res.send({ status: true, data }))
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});

router.post("/verify", (req, res) => {
  service
    .verify(req)
    .then((data) => res.send({ status: true, data }))
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});

router.get("/list", (req, res) => {
  service
    .list(req)
    .then((data) => res.send({ status: true, data }))
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});

router.get("/getById/:id", (req, res) => {
  service
    .get(req.params.id)
    .then((data) => res.send({ status: true, data }))
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});

router.post("/create", (req, res) => {
  service
    .create(req)
    .then((data) => res.status(200).send(data))
    .catch((err) => {
      res.status(400).send({
        status: false,
        error: err.message,
      });
    });
});

router.delete("/delete/:id", (req, res) => {
  service
    .delete(req.params.id)
    .then((data) => res.send({ status: true, data }))
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});

router.put("/edit/:id", (req, res) => {
  service
    .edit(req.params.id, req)
    .then((data) => res.send({ status: true, data }))
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});

module.exports = router;
