const mongoose = require("mongoose");
const argon2 = require("argon2");
const mongoosePaginate = require("mongoose-paginate-v2");

const deliveryPerson = new mongoose.Schema(
  {
    firstName: {
      type: String,
      required: true,
    },
    lastName: {
      type: String,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      unique: true,
    },
    phone: {
      type: String,
      trim: true,
    },
    phoneOtp: { type: String },
    role: {
      type: String,
      default: "delivery",
    },

    token: { type: String },
    editable: {
      type: Boolean,
      default: false,
    },
    active: {
      type: Boolean,
      default: false,
    },
    otp: {
      type: String,
    },
  },
  { timestamps: true }
);

deliveryPerson.plugin(mongoosePaginate);

deliveryPerson.pre("save", async function (next) {
  if (this.isModified("password")) {
    this.password = await argon2.hash(this.password);
  }
  next();
});

var deliveryUser = mongoose.model("delivery-users", deliveryPerson);

module.exports = {
  deliveryPerson: deliveryUser,
};
