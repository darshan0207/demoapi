var schema = require("./schema");
var jwt = require("jsonwebtoken");
const emailvalidator = require("email-validator");
const argon2 = require("argon2");
const sendEmail = require("../../../utils/emailService");
var mongoose = require("mongoose");
const { generateOTP } = require("../../../utils/otp");

module.exports = class deliveryUserService {
  login = async (req) => {
    const { email, password, isguest = false, role } = req.body;
    if (!email) {
      throw new Error("Please Enter Email");
    }

    if (!password && !isguest) {
      throw new Error("Please Enter password");
    }

    if (emailvalidator.validate(email)) {
      if (isguest) {
        let token = jwt.sign({ email: email }, "secret", {
          expiresIn: "1h",
        });
        return { token: token };
      } else {
        let payload = { email: email };
        if (role) {
          payload["role"] = role;
        }
        const userLogin = await schema.deliveryPerson.findOne(payload);
        if (userLogin && userLogin.active) {
          const isMatch = await argon2.verify(userLogin.password, password);
          if (isMatch) {
            delete userLogin.password;
            userLogin.token = jwt.sign({ email: email }, "secret", {
              expiresIn: "1h",
            });
            return userLogin;
          } else {
            throw new Error("Invalid Password");
          }
        } else {
          throw new Error("user Not Found");
        }
      }
    } else {
      throw new Error("Invalid Email");
    }
  };

  loginWithPhone = async (req) => {
    const { phone, password, isguest = false, role } = req.body;
    if (!phone) {
      throw new Error("Please Enter Mobile Number");
    }

    if (!password && !isguest) {
      throw new Error("Please Enter password");
    }

    if (isguest) {
      let token = jwt.sign({ phone: phone }, "secret", {
        expiresIn: "1h",
      });
      return { token: token };
    } else {
      const userLogin = await schema.deliveryPerson.findOne({
        email: email,
        role: role,
      });
      if (userLogin) {
        const isMatch = await argon2.verify(userLogin.password, password);
        if (isMatch) {
          delete userLogin.password;
          userLogin.token = jwt.sign({ email: email }, "secret", {
            expiresIn: "1h",
          });
          return userLogin;
        } else {
          throw new Error("Invalid Password");
        }
      } else {
        throw new Error("user Not Found");
      }
    }
  };

  signup = async (req) => {
    try {
      const { firstName, lastName, email, password } = req.body;
      if (!email) throw new Error("Please Enter Email");
      if (!password) throw new Error("Please Enter password");
      if (!firstName) throw new Error("Please Enter First Name");
      if (!lastName) throw new Error("Please Enter Last Name");

      if (!emailvalidator.validate(email)) {
        throw new Error("Invalid Email");
      }
      const userEmail = await schema.deliveryPerson.findOne({ email: email });
      if (userEmail) {
        throw new Error("Email already Exist");
      }
      let otp = await generateOTP(6);
      const signup = new schema.deliveryPerson({
        firstName: firstName,
        lastName: lastName,
        password: password,
        email: email,
        otp: otp,
      });
      await signup.save();
      return await sendEmail.email(email, otp);
    } catch (error) {
      throw new Error(error);
    }
  };

  forgetPassword = async (req) => {
    try {
      const email = req.body.email;
      if (!email) throw new Error("Please Enter Email");

      if (!emailvalidator.validate(email)) {
        throw new Error("Invalid Email");
      }
      // const userEmail = await schema.deliveryPerson.findOneAndUpdate(
      //   { email: email },
      //   { $set: { editable: true } },
      //   { upsert: false }
      // );
      // if (userEmail) {

      var token = jwt.sign({ email: email }, "secret", {
        expiresIn: "10m",
      });
      const link = `${process.env.WEB_URL}/reset-password?token=${token}`;
      return await sendEmail.forgetPasswordEmail(email, link);
      // } else {
      //   throw new Error("User Not Found...");
      // }
    } catch (error) {
      throw new Error(error);
    }
  };

  resetPassword = async (req) => {
    try {
      const password = req.body.password;
      try {
        let token = await jwt.verify(req.body.token, "secret");
        if (token.email) {
          if (!password) throw new Error("Please Enter password");
          const newpassword = await argon2.hash(password);
          const useredit = await schema.deliveryPerson.findOneAndUpdate(
            { email: token.email },
            { $set: { password: newpassword } },
            { upsert: false, select: { password: 0 } }
          );
          if (useredit) {
            return "your password has been reset successfully.";
          } else {
            throw new Error("Link not Available....");
          }
        }
      } catch (error) {
        throw new Error("Link not Available....");
      }
    } catch (error) {
      throw new Error(error);
    }
  };

  verify = async (req) => {
    try {
      const { email, otp } = req.body;
      const userEmail = await schema.deliveryPerson.findOne({ email: email });
      if (userEmail) {
        if (otp == userEmail.otp) {
          await schema.deliveryPerson.findOneAndUpdate(
            { email: email },
            { $set: { active: true, otp: "" } },
            { upsert: false }
          );
          return "You has been successfully registered";
        }
      } else {
        throw new Error("otp is incorrect");
      }
    } catch (error) {
      throw new Error(error);
    }
  };

  refreshToken = async (req) => {
    try {
      const { email } = req.body;
      return jwt.sign({ email: email }, "secret", {
        expiresIn: "1h",
      });
    } catch (error) {
      throw new Error(error);
    }
  };

  list = async (req) => {
    try {
      let page = parseInt(req.query.page) || 1;
      let limit = parseInt(req.query.limit) || 30;
      var query = {};
      var options = {
        select: "-password",
        limit: limit,
        page: page,
        lean: true,
      };
      return await schema.deliveryPerson.paginate(query, options);
    } catch (error) {
      throw new Error(error);
    }
  };

  get(id) {
    try {
      return schema.deliveryPerson.findOne({ _id: id }, { password: 0 });
    } catch (error) {
      throw new Error(error);
    }
  }

  async create(req) {
    try {
      const { firstName, lastName, email, password, role, active } = req.body;
      if (!email) throw new Error("Please Enter Email");
      if (!password) throw new Error("Please Enter password");
      if (!firstName) throw new Error("Please Enter First Name");
      if (!lastName) throw new Error("Please Enter Last Name");

      if (!emailvalidator.validate(email)) {
        throw new Error("Invalid Email");
      }
      const userEmail = await schema.deliveryPerson.findOne({ email: email });
      if (userEmail) {
        throw new Error("Email already Exist");
      }
      const signup = new schema.deliveryPerson({
        firstName: firstName,
        lastName: lastName,
        password: password,
        email: email,
        role: role,
        active: active,
      });
      return await signup.save();
    } catch (error) {
      throw new Error(error);
    }
  }

  async delete(id) {
    try {
      return await schema.deliveryPerson.deleteOne({ _id: id });
    } catch (error) {
      throw new Error(error);
    }
  }

  edit = async (id, req) => {
    try {
      if (req.body.password) {
        req.body.password = await argon2.hash(req.body.password);
      }
      return await schema.deliveryPerson.findOneAndUpdate(
        { _id: id },
        { $set: req.body },
        { upsert: false, new: true, select: { password: 0 } }
      );
    } catch (error) {
      throw new Error(error);
    }
  };
};
