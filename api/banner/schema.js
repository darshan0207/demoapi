var mongoose = require("mongoose");
var banner = new mongoose.Schema({
  image: {
    type: String,
    required: true,
  },
  title: {
    type: String,
    required: true,
  },
  second_title: {
    type: String,
  },
  button_name: {
    type: String,
  },
  button_link: {
    type: String,
  },
  active: {
    type: Boolean,
    default: true,
  },
});

var banners = mongoose.model("banners", banner);

module.exports = {
  banner: banners,
};
