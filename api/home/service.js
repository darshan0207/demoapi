var schema = require("./schema");
var fs = require("fs");
const multer = require("multer");
// const path = require("path");
// const upload = multer({ dest: "public/files" });

// const upload = multer({ storage: storage }).single("image");

module.exports = class homeService {
  get() {
    try {
      return schema.home
        .findOne({ _id: 1 })
        .populate(["topproduct", "trendingitem"]);
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  async edit(req) {
    try {
      // if (req.body && req.body.second_banner && req.body.second_banner.image) {
      //   let banner2 = "/public/uploads-" + Math.random();
      //   await getImageGenerator(req.body.second_banner.image, "." + banner2);
      //   req.body.second_banner.image =
      //     banner2 +
      //     "." +
      //     req.body.second_banner.image.substring(
      //       "data:image/".length,
      //       req.body.second_banner.image.indexOf(";base64")
      //     );
      // }

      // if (req.body && req.body.third_banner && req.body.third_banner.image) {
      //   let banner2 = "/public/uploads-" + Math.random();
      //   await getImageGenerator(req.body.third_banner.image, "." + banner2);
      //   req.body.third_banner.image =
      //     banner2 +
      //     "." +
      //     req.body.third_banner.image.substring(
      //       "data:image/".length,
      //       req.body.third_banner.image.indexOf(";base64")
      //     );
      // }

      return schema.home
        .findOneAndUpdate(
          { _id: 1 },
          { $set: req.body },
          { upsert: true, new: true }
        )
        .populate(["topproduct", "trendingitem"]);
      // });
    } catch (err) {
      console.log(err);
      throw new Error(err);
    }
  }
  nmv;
};

function getImageGenerator(base64Data, image) {
  return new Promise((resolve, reject) => {
    if (base64Data.substring(0, 4) == "data") {
      let imagePath =
        image +
        "." +
        base64Data.substring(
          "data:image/".length,
          base64Data.indexOf(";base64")
        );
      var data = base64Data.replace(/^data:image\/\w+;base64,/, "");
      var buf = new Buffer(data, "base64");
      fs.writeFile(imagePath, buf, "base64", function (err) {
        if (err) {
          // console.log(err);
          return reject(err);
        } else {
          return resolve(imagePath);
        }
      });
    } else {
      return resolve(base64Data);
    }
  });
}
