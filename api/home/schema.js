var mongoose = require("mongoose");
var home = new mongoose.Schema({
  _id: { type: Number, default: 1 },
  banner: [{ type: String }],
  second_banner: {
    image: {
      type: String,
    },
    title: {
      type: String,
    },
    second_title: {
      type: String,
    },
    button_name: {
      type: String,
    },
    button_link: {
      type: String,
    },
    active: {
      type: Boolean,
      default: true,
    },
  },
  third_banner: {
    image: {
      type: String,
    },
    title: {
      type: String,
    },
    second_title: {
      type: String,
    },
    button_name: {
      type: String,
    },
    button_link: {
      type: String,
    },
    active: {
      type: Boolean,
      default: true,
    },
  },
  topproduct: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "products",
    },
  ],
  trendingitem: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "products",
    },
  ],
});

var homes = mongoose.model("homes", home);

module.exports = {
  home: homes,
};
