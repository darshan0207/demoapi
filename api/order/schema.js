var mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");
const AutoIncrement = require("mongoose-sequence")(mongoose);

var order = new mongoose.Schema(
  {
    // orderId: { type: String, required: true },
    // itemtotal: { type: String, required: true },
    // total: { type: String, required: true },
    // billaddress: {
    //   countryid: {
    //     type: mongoose.Schema.Types.ObjectId,
    //     ref: "countrys",
    //   },
    //   stateid: {
    //     type: mongoose.Schema.Types.ObjectId,
    //     ref: "states",
    //   },
    //   address1: { type: String },
    //   address2: { type: String },
    //   city: { type: String },
    //   zipcode: { type: String },
    //   phone: { type: String },
    //   firstname: { type: String },
    //   lastname: { type: String },
    //   company: { type: String },
    // },
    // shipaddress: {
    //   countryid: {
    //     type: mongoose.Schema.Types.ObjectId,
    //     ref: "countrys",
    //   },
    //   stateid: {
    //     type: mongoose.Schema.Types.ObjectId,
    //     ref: "states",
    //   },
    //   address1: { type: String },
    //   address2: { type: String },
    //   city: { type: String },
    //   zipcode: { type: String },
    //   phone: { type: String },
    //   firstname: { type: String },
    //   lastname: { type: String },
    //   company: { type: String },
    // },
    // shipmentstate: {
    //   type: String,
    //   enum: ["backorder", "canceled", "partial", "pending", "ready", "shipped"],
    //   default: "backorder",
    // },
    // paymentstate: {
    //   type: String,
    //   enum: ["balance_due", "credit_owed", "failed", "paid", "void"],
    //   default: "balance_due",
    // },
    // paymentmethod: { type: mongoose.Schema.Types.ObjectId, ref: "paymentMethod" },
    // paymenttotal: { type: String },
    // paymentid: { type: String },
    // shipmentmethod: {
    //   type: mongoose.Schema.Types.ObjectId,
    //   ref: "shippingCategory",
    // },
    // shipmenttotal: { type: String },
    // email: { type: String, required: true },
    // currency: { type: String, default: "$" },
    // lastipaddress: { type: String },
    // additionaltaxtotal: { type: String },
    // promototal: { type: String },
    // includedtaxtotal: { type: String },
    // itemcount: { type: String },
    // approverid: { type: String },
    // confirmationdelivered: { type: Boolean, default: false },
    // consideredrisky: { type: Boolean, default: false },
    // cancelerid: { type: String },
    // taxableadjustmenttotal: { type: String },
    // nontaxableadjustmenttotal: { type: String },
    // storeownernotificationdelivered: { type: Boolean, default: false },
    // orderdate: { type: Date, default: Date.now },
    // state: {
    //   type: String,
    //   enum: ["cart", "address", "delivery", "payment", "confirm", "complete"],
    //   default: "cart",
    // },
    // nextstep: {
    //   type: String,
    //   enum: ["cart", "address", "delivery", "payment", "confirm", "complete"],
    // },
    // lineitems: [
    //   {
    //     product: { type: Object },
    //     quantity: { type: String },
    //   },
    // ],
    orderId: { type: Number },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "signups",
      required: true,
    },
    Product: [
      {
        type: Object,
      },
    ],
    diningMode: { type: String },
    eta: {
      prefixText: { type: String },
      rangeText: { type: String },
      scheduleText: { type: String },
    },
    deliveryAddress: {
      type: Object,
    },
    deliverytax: { type: String },
    othertax: { type: String },
    addtip: { type: String },
    orderdate: { type: Date, default: Date.now },
    confirmationdelivered: { type: Boolean, default: false },
    payment: {
      type: Object,
    },
    total: { type: String, required: true },
    state: {
      type: String,
      enum: [
        "placed",
        "confirmed",
        "processed",
        "ready",
        "delivery",
        "pickup",
        "delivered",
        "cancel",
      ],
      default: "placed",
    },
    includesilverware: { type: Boolean, default: false },
    instructions: { type: String },
    somepickup: { type: Boolean, default: false },
    pickupname: { type: String },
    logs: [
      {
        status: {
          type: String,
          default: "placed",
        },
        allowCancellation: {
          type: Boolean,
          default: true,
        },
        adminNotification: {
          type: Boolean,
          default: false,
        },
        customerNotification: {
          type: Boolean,
          default: false,
        },
        updatedby: {
          type: mongoose.Schema.Types.ObjectId,
          ref: "signups",
        },
        updateddate: {
          type: Date,
          default: Date,
        },
      },
    ],
    deliveryby: { type: mongoose.Schema.Types.ObjectId, ref: "signups" },
  },
  { timestamps: true }
);
order.plugin(AutoIncrement, {
  inc_field: "orderId",
});
order.plugin(mongoosePaginate);

var orders = mongoose.model("orders", order);

module.exports = {
  order: orders,
};
