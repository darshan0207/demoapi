const express = require("express");
const router = express.Router();
const Service = require("./service");
const service = new Service();

router.get("/", (req, res) => {
  service
    .list(req)
    .then((data) => res.send({ status: true, data }))
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});

router.get("/:id", (req, res) => {
  service
    .get(req.params.id)
    .then((data) => res.send({ status: true, data }))
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});

router.post("/", (req, res) => {
  service
    .create(req)
    .then((data) => res.status(200).send({ status: true, data }))
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});

router.put("/:id", (req, res) => {
  service
    .edit(req.params.id, req)
    .then((data) => res.send({ status: true, data }))
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});

router.post("/cardxPayment", (req, res) => {
  service
    .cardxPayment(req)
    .then((data) => res.send({ status: true, data }))
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err,
      })
    );
});

router.post("/orderstatuslog", (req, res) => {
  service
    .orderstatuslog(req)
    .then((data) => res.send({ status: true, data }))
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err,
      })
    );
});

module.exports = router;
