var schema = require("./schema");
var cartSchema = require("../cart/schema");
const taxRateService = require("../taxrate/service");
const request = require("request");
const app = require("../../index");
const stripe = require("stripe")(
  "sk_test_51HFZKmE4pgx94R1PNoihUs6ZlYPOrzjXpA8rcogdbEQWOfeFhWbD1gvPfVjVvX1GFaxzZZUmmCYXEEnVJrnFFEz400a5sHM2n7"
);
// console.log("getSocketIo", getSocketIo);
// getSocketIo.emit("test", "test");
module.exports = class orderService {
  constructor() {
    this.taxRateService = new taxRateService();
  }
  async list(req) {
    try {
      let page = parseInt(req.query.page) || 1;
      let limit = parseInt(req.query.limit) || 30;

      var query = {};
      if (req.query.user) query["user"] = req.query.user;

      if (req.query.isstaff) {
        if (req.query.isprevious) {
          query["state"] = {
            $in: ["ready", "delivery", "pickup", "delivered", "cancel"],
          };
        } else {
          query["state"] = {
            $in: ["placed", "confirmed", "processed"],
          };
        }
      }

      if (req.query.isdelivery) {
        if (req.query.isprevious) {
          query["deliveryby"] = req.query.userid;
          query["diningMode"] = "DELIVERY";
          query["state"] = "delivered";
        } else {
          query["$and"] = [];
          query["$and"].push(
            {
              $or: [
                { deliveryby: { $exists: false } },
                { deliveryby: req.query.userid },
              ],
            },
            { diningMode: "DELIVERY" },
            { state: { $in: ["ready", "delivery"] } }
          );
        }
      }

      var options = {
        limit: limit,
        page: page,
        populate: ["user"],
        lean: true,
        sort: { orderdate: "desc" },
      };
      return await schema.order.paginate(query, options);
    } catch (error) {
      throw new Error(error);
    }
  }

  async get(id) {
    try {
      return schema.order.findOne({ orderId: id }).populate(["user"]);
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  async create(req) {
    try {
      const data = new schema.order(req.body);
      let res = await data.save().then((t) => t.populate("user"));
      const io = app.getSocketIo();
      // io.emit("orderplace", "A new order has been placed.");
      io.emit("orderplace", res);
      // getSocketIo.emit("orderplace", "hello");
      return await res;
    } catch (err) {
      throw new Error(err);
    }
  }

  cardxPayment = (req) => {
    return new Promise((resolve, reject) => {
      var options = {
        method: "POST",
        url: `https://test.api.paywithcardx.com/api/merchant/:${process.env.ACCOUNT_NAME}/order/transaction`,
        headers: {
          Accept: "application/json",
          "X-Gateway-Account": process.env.ACCOUNT_NAME,
          "X-Gateway-Api-Key-Name": process.env.ACCOUNT_API_NAME,
          "X-Gateway-Api-Key": process.env.ACCOUNT_API_KEY,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          transactions: {
            transaction1: {
              billingInfo: {
                name: req.body.FirstName + " " + req.body.lastName,
                // address: req.body.billingAddress.AddressLine1,
                // city: req.body.billingAddress.City,
                // state: req.body.billingAddress.State,
                // postalCode: req.body.billingAddress.Zipcode,
                // country: req.body.billingAddress.Country,
                // phone: req.body.ContactNo,
                email: req.body.email,
              },
              payment: {
                card: {
                  number: req.body.cardNumber,
                  expMonth: req.body.expirationMonth,
                  expYear: req.body.expirationYear,
                  cvv: req.body.cardCode,
                },
                mode: "authorization",
                type: "card",
              },
              amount: req.body.amount,
              currency: "usd",
              // security: {
              //   ipAddress: req.body.ip,
              // },
              processMode: "sync",
              // customData: req.body.customData,
            },
          },
        }),
      };
      request(options, function (error, response, body) {
        if (!body) {
          return reject(error);
        } else {
          let data = JSON.parse(body);
          if (
            data.content &&
            data.content.data &&
            data.content.data.errors &&
            data.content.data.errors.transaction1 &&
            data.content.data.errors.transaction1.message
          ) {
            return reject(data.content.data.errors.transaction1.message);
          } else {
            return resolve(data.content.data);
          }
        }
      });
    });
  };

  // async create(req) {
  //   try {
  //     let cartItem = await cartSchema.cart
  //       .find({
  //         email: req.data.user.email,
  //       })
  //       .populate("productId", { _id: 0, __v: 0 })
  //       .lean();
  //     if (cartItem.length <= 0) {
  //       throw new Error("The resource you were looking for could not be found");
  //     }
  //     let itemtotal = 0;
  //     let totaltax = 0;
  //     let lineitem = cartItem.map(async (data) => {
  //       itemtotal += data.quantity * data.productId.price;
  //       if (
  //         req.body.shipaddress &&
  //         data &&
  //         data["productId"] &&
  //         data["productId"].taxcategory
  //       ) {
  //         data["productId"]["taxrate"] = await this.gettax(
  //           data["productId"].taxcategory,
  //           req.body.shipaddress
  //         );
  //         totaltax += parseFloat(data["productId"]["taxrate"]);
  //       }
  //       return {
  //         product: data["productId"],
  //         quantity: data.quantity,
  //       };
  //     });
  //     req.body.itemtotal = itemtotal;
  //     req.body.total = itemtotal;
  //     req.body.lineitems = await Promise.all(lineitem);
  //     req.body.additionaltaxtotal = totaltax;

  //     let findorder = await schema.order
  //       .findOne({
  //         email: req.data.user.email,
  //         state: { $in: ["cart", "address", "delivery", "payment"] },
  //       })
  //       .populate(["shipaddress.countryid", "shipaddress.stateid"]);
  //     if (!findorder || !findorder.orderId) {
  //       try {
  //         let documentCount = await schema.order.countDocuments();
  //         req.body.orderId = documentCount + 1;
  //         req.body.email = req.data.user.email;
  //         const data = new schema.order(req.body);
  //         const result = await data.save();
  //         if (result) {
  //           return result;
  //         } else {
  //           throw new Error(
  //             "The resource you were looking for could not be found"
  //           );
  //         }
  //       } catch (err) {
  //         throw new Error(
  //           "The resource you were looking for could not be found"
  //         );
  //       }
  //     } else {
  //       const ordernextstep = {
  //         address: "delivery",
  //         delivery: "payment",
  //         payment: "confirm",
  //         confirm: "complete",
  //       };
  //       req.body.nextstep = ordernextstep[req.body.state] ?? "address";

  //       if (req.body.state == "payment") {
  //         try {
  //           console.log(stripe);
  //           const payment = await stripe.paymentIntents.create({
  //             amount: itemtotal.toFixed(0),
  //             currency: "USD",
  //             description: "test",
  //             payment_method: req.body.paymentid,
  //             confirm: true,
  //             payment_method_types: ["card"],
  //             confirmation_method: "automatic",
  //             capture_method: "automatic",
  //             shipping: {
  //               name: findorder.email,
  //               address: {
  //                 line1: findorder.shipaddress.address1,
  //                 postal_code: findorder.shipaddress.zipcode,
  //                 city: findorder.shipaddress.city,
  //                 state: findorder.shipaddress.stateid.iso,
  //                 country: findorder.shipaddress.countryid.iso,
  //               },
  //             },
  //           });
  //           console.log("stripe2");
  //           if (payment && payment.id) {
  //             req.body.paymentid = payment.id;
  //             req.body.paymentstate = "paid";
  //           } else {
  //             throw new Error("payment unsuccessful");
  //           }
  //         } catch (error) {
  //           throw new Error(error);
  //         }
  //       } else if (req.body.state == "complete") {
  //         await cartSchema.cart.deleteMany({
  //           email: req.data.user.email,
  //         });
  //       }
  //       return schema.order
  //         .findOneAndUpdate(
  //           { orderId: findorder.orderId },
  //           { $set: req.body },
  //           { upsert: false, new: true }
  //         )
  //         .populate(["shipmentmethod", "paymentmethod"]);
  //     }
  //   } catch (error) {
  //     throw new Error(error);
  //   }
  // }

  async edit(id, req) {
    try {
      const result = await schema.order
        .findOneAndUpdate(
          { _id: id },
          { $set: req.body },
          { upsert: false, new: true }
        )
        .populate("user");
      const io = app.getSocketIo();
      io.emit("orderaccept", result);
      return await result;
    } catch (error) {
      throw new Error(error);
    }
  }

  async gettax(id, data) {
    const result = await this.taxRateService.getzonecategory(id);
    if (result) {
      let zone = result.zone.kind == "state" ? data.stateid : data.countryid;
      const index = result.zone.zonemembers.findIndex((data) => data === zone);
      if (index === -1) {
        return 0;
      }
      return result.rate ?? 0;
    }
  }

  orderstatuslog = async (req) => {
    try {
      const result = await schema.order
        .findOneAndUpdate(
          {
            _id: req.body.id,
          },
          {
            state: req.body.state,
            $push: {
              logs: {
                $each: req.body.logs,
              },
            },
          },
          {
            upsert: false,
            new: true,
          }
        )
        .populate("user");
      const io = app.getSocketIo();
      io.emit("orderstatuschange", result);
      return result;
      // if (req.body.logs[0].adminNotification) {
      //       // console.log(req.body.logs[0])
      //       var message =
      //         "order item " +
      //         req.body.clonedObj.items.jobid +
      //         " is " +
      //         req.body.logs[0].customerStatus +
      //         " of order id " +
      //         req.body.clonedObj.vibrantOrderId;
      //       notifications.create({
      //         message: message,
      //         route: "Orders/orderview",
      //         read: false,
      //       });
      //       sendMail.orderStatusUpdateMail(req.body, req.body.logs[0], true);
      //     }
      //     if (req.body.logs[0].customerNotification) {
      //       sendMail.orderStatusUpdateMail(req.body, req.body.logs[0], false);
      //     }
    } catch (error) {
      throw new Error(error);
    }
  };
};
