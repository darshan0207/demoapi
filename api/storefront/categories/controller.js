const express = require("express");
const router = express.Router();
const Service = require("./service");
const service = new Service();

router.get("/", (req, res) => {
  service
    .list(req)
    .then((data) => res.send({ status: true, data }))
    .catch((err) =>
      res.status(400).send({
        status: false,
        error: err.message,
      })
    );
});

module.exports = router;
