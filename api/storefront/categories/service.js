var schema = require("../../taxon/schema");

module.exports = class categoriesService {
  async list() {
    let filter = {
      is_child: true,
      depth: 1,
    };
    try {
      // return await schema.taxon.find(filter).sort({ position: 1 });

      let data = await schema.taxon.find().sort({ position: 1 });
      return await deserializeCategories(data);
      // return await categories.find((taxon) => !taxon.parent_id);
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }
};

const findItems = (taxon, apiTaxons) => {
  const taxonIds = taxon.id;
  const items = apiTaxons.filter((taxon) => taxon.parent_id == taxonIds);
  return items.map((item) => ({
    id: item.id,
    name: item.name,
    slug: item.permalink,
    items: findItems(item, apiTaxons),
    parent_id: taxon.parent_id,
    is_root: taxon.is_root,
  }));
};

const deserializeCategories = (apiTaxons) =>
  apiTaxons.map((taxon) => ({
    id: taxon.id,
    name: taxon.name,
    slug: taxon.permalink,
    items: findItems(taxon, apiTaxons),
    parent_id: taxon.parent_id,
    is_root: taxon.is_root,
  }));
