require("dotenv").config({ debug: process.env.DEBUG });
exports.userLogin = async (req, res) => {
  try {
    const file = req.file;
    console.log(file);
    if (!file) {
      res.status(400).send({
        status: false,
        error: "Please upload a file",
      });
      return;
    }
    res
      .status(200)
      .send({ status: true, data: process.env.assets + "/" + file.filename });
  } catch (err) {
    res.status(400).send({
      status: false,
      error: err.message,
    });
  }
};
