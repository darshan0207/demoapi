var mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");

var address = new mongoose.Schema({
  countryid: { type: String },
  countryiso: { type: String },
  stateid: { type: String },
  stateiso: { type: String },
  address1: { type: String, required: true },
  address2: { type: String },
  city: { type: String },
  zipcode: { type: String },
  phone: { type: String },
  alternativephone: { type: String },
  firstname: { type: String },
  lastname: { type: String },
  label: { type: String },
  company: { type: String },
  email: { type: String },
  shipping: { type: Boolean, default: false },
  billing: { type: Boolean, default: false },
  name: { type: String },
  latitude: { type: Number },
  longitude: { type: Number },
  instruction: {
    aptorsuite: { type: String },
    notes: { type: String },
    businessname: { type: String },
    interactiontype: { type: String },
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "signups",
    required: true,
  },
});
address.plugin(mongoosePaginate);

// address.pre("save", async function (next) {
//   var filter = {};
//   if (this.shipping) {
//     filter["shipping"] = false;
//   }
//   if (this.billing) {
//     filter["billing"] = false;
//   }

//   if (Object.keys(filter).length) {
//     mongoose.models["addresses"]
//       .update({ email: this.email }, { $set: filter }, { multi: true })
//       .then((data) => {
//         next();
//       });
//   } else {
//     next();
//   }
// });

// address.pre("findOneAndUpdate", async function (next) {
//   var filter = {};
//   if (this._update && this._update.$set.shipping) {
//     filter["shipping"] = false;
//   }
//   if (this._update && this._update.$set.billing) {
//     filter["billing"] = false;
//   }

//   if (Object.keys(filter).length) {
//     mongoose.models["addresses"]
//       .update(
//         { email: this._update && this._update.$set.email },
//         { $set: filter },
//         { multi: true }
//       )
//       .then((data) => {
//         next();
//       });
//   } else {
//     next();
//   }
// });

var addresses = mongoose.model("addresses", address);

module.exports = {
  address: addresses,
};
