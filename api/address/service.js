var schema = require("./schema");

module.exports = class addressService {
  async list(req) {
    try {
      let page = parseInt(req.query.page) || 1;
      let limit = parseInt(req.query.limit) || 30;
      var query = {};
      if (req.query.email) query["email"] = req.query.email;
      if (req.query.id) query["user"] = req.query.id;
      var options = {
        limit: limit,
        page: page,
        lean: true,
      };
      return await schema.address.paginate(query, options);
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  get(id) {
    try {
      return schema.address.find({ user: id });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  findById(id) {
    try {
      return schema.address.findOne({ _id: id });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  create(req) {
    try {
      const data = new schema.address(req.body);
      return data.save();
    } catch (err) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  edit(id, req) {
    try {
      return schema.address.findOneAndUpdate(
        { _id: id },
        { $set: req.body },
        { upsert: false, new: true }
      );
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  delete(id) {
    try {
      return schema.address.deleteOne({ _id: id });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }
};
