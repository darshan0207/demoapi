const mongoose = require("mongoose");
const argon2 = require("argon2");
const mongoosePaginate = require("mongoose-paginate-v2");

const signup = new mongoose.Schema(
  {
    firstName: {
      type: String,
      required: true,
    },
    lastName: {
      type: String,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      unique: true,
    },
    phone: {
      type: String,
      trim: true,
    },
    phoneOtp: { type: String },
    role: {
      type: String,
      default: "user",
    },

    token: { type: String },
    editable: {
      type: Boolean,
      default: false,
    },
    wishlist: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "products",
      },
    ],
    active: {
      type: Boolean,
      default: false,
    },
    otp: {
      type: String,
    },
  },
  { timestamps: true }
);

signup.plugin(mongoosePaginate);

signup.pre("save", async function (next) {
  if (this.isModified("password")) {
    this.password = await argon2.hash(this.password);
  }
  next();
});

var signups = mongoose.model("signups", signup);

module.exports = {
  signup: signups,
};
