var schema = require("./schema");

module.exports = class stockLocationService {
  list(req) {
    try {
      return schema.stockLocation.find();
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  get(id) {
    try {
      return schema.stockLocation.findOne({ _id: id });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  create(req) {
    try {
      return schema.stockLocation
        .findOne({
          name: req.body.name,
        })
        .then(async (stockLocation) => {
          if (stockLocation && stockLocation.name) {
            throw new Error("Stock location already exist");
          } else {
            const data = new schema.stockLocation(req.body);
            return await data.save();
          }
        });
    } catch (err) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  delete(id) {
    try {
      return schema.stockLocation.deleteOne({ _id: id });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  edit(id, data) {
    try {
      return schema.stockLocation.findOneAndUpdate(
        { _id: id },
        { $set: data },
        { upsert: false, new: true }
      );
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }
};
