var mongoose = require("mongoose");
var stockLocation = new mongoose.Schema({
  name: { type: String, required: true },
  internalname: { type: String },
  address1: { type: String },
  address2: { type: String },
  city: { type: String },
  country: { type: String },
  state: { type: String },
  zip: { type: String },
  phone: { type: String },
  active: { type: Boolean },
  default: { type: Boolean },
  backorderabledefault: { type: Boolean },
  propagateallvariants: { type: Boolean },
});

var stockLocations = mongoose.model("stocklocations", stockLocation);

module.exports = {
  stockLocation: stockLocations,
};
