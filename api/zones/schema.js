var mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");

var zone = new mongoose.Schema({
  name: { type: String, required: true },
  description: { type: String },
  defaulttax: { type: Boolean, default: false },
  includprice: { type: Boolean, default: false },
  kind: {
    type: String,
    enum: ["state", "country"],
    default: "state",
  },
  zonemembers: {
    type: Array,
  },
});

zone.plugin(mongoosePaginate);

var zones = mongoose.model("zones", zone);

module.exports = {
  zone: zones,
};
