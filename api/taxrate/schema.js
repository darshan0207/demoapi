var mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");

var taxrate = new mongoose.Schema({
  name: { type: String, required: true },
  ratedelivery: { type: Number },
  ratepickup: { type: Number },
  ratepreserve: { type: Number },
  // rate: { type: String, required: true },
  // zone: {
  //   type: mongoose.Schema.Types.ObjectId,
  //   ref: "zones",
  // },
  // taxcategory: {
  //   type: mongoose.Schema.Types.ObjectId,
  //   ref: "taxcategorys",
  //   required: true,
  // },
  // showrate: { type: Boolean, default: true },
  // includprice: { type: Boolean, default: false },
  // calculatorattributes: {
  //   type: { type: String },
  //   preferences: { type: String },
  // },
});
taxrate.plugin(mongoosePaginate);

var taxrates = mongoose.model("taxrates", taxrate);

module.exports = {
  taxrate: taxrates,
};
