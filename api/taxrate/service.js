var schema = require("./schema");

module.exports = class taxrateService {
  async list(req) {
    try {
      // if (req.query.admin) {
      //   let page = parseInt(req.query.page) || 1;
      //   let limit = parseInt(req.query.limit) || 30;

      //   var query = {};
      //   var options = {
      //     limit: limit,
      //     page: page,
      //     lean: true,
      //     populate: ["taxcategory", "zone"],
      //   };
      //   return await schema.taxrate.paginate(query, options);
      // } else {
      return await schema.taxrate.find();
      // }
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  get(id) {
    try {
      return schema.taxrate.findOne({ _id: id }).populate(["zone"]);
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  getzonecategory(id) {
    try {
      return schema.taxrate.findOne({ taxcategory: id }).populate(["zone"]);
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  create(req) {
    try {
      const data = new schema.taxrate(req.body);
      return data.save();
    } catch (err) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  edit(id, data) {
    try {
      return schema.taxrate.findOneAndUpdate(
        { _id: id },
        { $set: data },
        { upsert: false, new: true }
      );
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  delete(id) {
    try {
      return schema.taxrate.deleteOne({ _id: id });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }
};

const defaultTaxes = async () => {
  return await schema.taxrate.updateOne(
    { name: "Food" },
    {
      $set: {
        name: "Food",
        ratedelivery: "1",
        ratepickup: "1",
        ratepreserve: "1",
      },
    },
    { upsert: true }
  );
};
defaultTaxes();
