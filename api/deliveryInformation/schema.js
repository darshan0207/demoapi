var mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");

var deliveryInformation = new mongoose.Schema({
  title: { type: String, required: true },
  titlebadge: { type: String },
  subtitle: { type: String },
  type: { type: String, required: true },
  description: { type: String, required: true },
  active: { type: Boolean, default: true },
  position: { type: Number, default: 0 },
});

deliveryInformation.plugin(mongoosePaginate);

var deliveryInformations = mongoose.model(
  "deliveryInformations",
  deliveryInformation
);

module.exports = {
  deliveryInformation: deliveryInformations,
};
