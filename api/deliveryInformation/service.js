var schema = require("./schema");

module.exports = class DeliveryInformationService {
  async list(req) {
    try {
      if (req.query.admin) {
        let page = parseInt(req.query.page) || 1;
        let limit = parseInt(req.query.limit) || 30;
        var query = {};
        var options = {
          limit: limit,
          page: page,
          lean: true,
        };
        return await schema.deliveryInformation.paginate(query, options);
      } else {
        return await schema.deliveryInformation
          .find({ active: true })
          .sort({ position: 1 });
      }
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  get(id) {
    try {
      return schema.deliveryInformation.findOne({ _id: id });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  create(req) {
    try {
      return schema.deliveryInformation
        .findOne({
          title: req.body.title,
          type: req.body.type,
        })
        .then(async (item) => {
          if (item && item.title) {
            throw new Error("Delivery Information already exist");
          } else {
            const data = new schema.deliveryInformation(req.body);
            return await data.save();
          }
        });
    } catch (err) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  delete(id) {
    try {
      return schema.deliveryInformation.deleteOne({ _id: id });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  edit(id, data) {
    try {
      return schema.deliveryInformation.findOneAndUpdate(
        { _id: id },
        { $set: data },
        { upsert: false, new: true }
      );
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }
};
