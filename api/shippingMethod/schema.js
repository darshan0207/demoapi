var mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");

var shippingMethod = new mongoose.Schema({
  name: { type: String, required: true },
  adminname: { type: String },
  code: { type: String, required: true },
  trackingurl: { type: String, required: true },
  displayon: {
    type: String,
    enum: ["both", "backend", "frontend"],
    default: "both",
  },
  taxcategoryid: { type: String },
  shippingcategoryids: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "shippingCategory",
    },
  ],
  calculatorattributes: { type: String },
});

shippingMethod.plugin(mongoosePaginate);

var shippingMethods = mongoose.model("shippingmethod", shippingMethod);

module.exports = {
  shippingMethod: shippingMethods,
};
