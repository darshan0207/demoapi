var mongoose = require("mongoose");
var currency = new mongoose.Schema({
  name: { type: String, required: true },
  presentation: { type: String, required: true },
  active: { type: Boolean, default: true },
});

var currencys = mongoose.model("currencys", currency);

module.exports = {
  currency: currencys,
};
