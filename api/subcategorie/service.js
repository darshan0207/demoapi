var schema = require("./schema");

module.exports = class subCategorieService {
  async list(req) {
    try {
      var query = {};
      if (req && req.query.admin) {
        let page = parseInt(req.query.page) || 1;
        let limit = parseInt(req.query.limit) || 30;
        if (req.query.categorie_id) {
          query["categorie_id"] = req.query.categorie_id;
        }

        var options = {
          limit: limit,
          page: page,
          lean: true,
        };
        return await schema.subcategorie.paginate(query, options);
      } else {
        if (req.query.categorie_id) {
          query["categorie_id"] = req.query.categorie_id;
        }
        query["active"] = true;
        return await schema.subcategorie.find(query).lean();
      }
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  getByCategory(id) {
    try {
      return schema.subcategorie.find({ categorie_id: id }).lean();
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  // list(id) {
  //   try {
  //     var query = {};
  //     if (id) {
  //       query["categorie_id"] = id;
  //     }
  //     return schema.subcategorie.find(query).lean();
  //   } catch (error) {
  //     console.log(error);
  //     throw new Error("The resource you were looking for could not be found");
  //   }
  // }

  get(id) {
    try {
      return schema.subcategorie.findOne({ _id: id });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  create(req) {
    try {
      return schema.subcategorie
        .findOne({
          name: req.body.name,
        })
        .then(async (subcategorie) => {
          if (subcategorie && subcategorie.name) {
            throw new Error("Sub Categorie already exist");
          } else {
            const data = new schema.subcategorie(req.body);
            return await data.save();
          }
        });
    } catch (err) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  delete(id) {
    try {
      return schema.subcategorie.deleteOne({ _id: id });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  edit(id, data) {
    try {
      return schema.subcategorie.findOneAndUpdate(
        { _id: id },
        { $set: data },
        { upsert: false, new: true }
      );
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }
};
