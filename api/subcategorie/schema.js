var mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");

var subcategorie = new mongoose.Schema({
  categorie_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "categories",
  },
  name: { type: String, required: true },
  slug: { type: String, required: true },
  position: { type: Number, default: 0 },
  description: { type: String },
  meta_title: { type: String },
  meta_description: { type: String },
  meta_keywords: { type: String },
  seo_title: { type: String },
  image: { type: String },
  active: { type: Boolean, default: true },
});

subcategorie.plugin(mongoosePaginate);
var subcategories = mongoose.model("subcategories", subcategorie);

module.exports = {
  subcategorie: subcategories,
};
