var mongoose = require("mongoose");
var optionValue = new mongoose.Schema({
  name: { type: String, required: true },
  presentation: { type: String },
  price: { type: String },
  sort: { type: Number, default: 0 },
  is_default: { type: Boolean, default: true },
  optiontypeid: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "optiontypes",
    required: true,
  },
  restaurant_menu_availabilities: { type: String },
});

var optionValues = mongoose.model("optionvalues", optionValue);

module.exports = {
  optionValue: optionValues,
};
