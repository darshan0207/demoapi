var schema = require("./schema");

module.exports = class OptionValueService {
  list(req) {
    try {
      return schema.optionValue.find();
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  findAll(id) {
    try {
      return schema.optionValue.find({ optiontypeid: id });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  get(id) {
    try {
      return schema.optionValue.findOne({ _id: id });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  create(req) {
    try {
      return schema.optionValue
        .findOne({
          name: req.body.name,
        })
        .then(async (optionValue) => {
          if (optionValue && optionValue.name) {
            throw new Error("Option value already exist");
          } else {
            const data = new schema.optionValue(req.body);
            return await data.save();
          }
        });
    } catch (err) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  delete(id) {
    try {
      return schema.optionValue.deleteOne({ _id: id });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  edit(id, data) {
    try {
      return schema.optionValue.findOneAndUpdate(
        { _id: id },
        { $set: data },
        { upsert: false, new: true }
      );
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }
};
