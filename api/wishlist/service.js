const userSchema = require("../auth/schema");

module.exports = class wishlistService {
  async list(req) {
    try {
      return userSchema.signup
        .find({ email: req.data.user.email }, { wishlist: 1 })
        .populate(["wishlist"]);
    } catch (error) {
      throw new Error(error);
    }
  }

  async create(req) {
    try {
      const data = await userSchema.signup.findOne({
        email: req.data.user.email,
      });
      let index = data.wishlist.indexOf(req.body.productId);
      index === -1
        ? data.wishlist.push(req.body.productId)
        : data.wishlist.splice(index, 1);
      return await data.save();
    } catch (error) {
      throw new Error(error);
    }
  }
};
