var mongoose = require("mongoose");
var protoType = new mongoose.Schema({
  name: { type: String, required: true },
  properties: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "properties",
    },
  ],
  optiontypes: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "optiontypes",
    },
  ],
  taxonomies: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "taxonomies",
    },
  ],
});

var protoTypes = mongoose.model("prototypes", protoType);

module.exports = {
  protoType: protoTypes,
};
