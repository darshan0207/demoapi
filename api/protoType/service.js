var schema = require("./schema");

module.exports = class protoTypeService {
  list(req) {
    try {
      return schema.protoType
        .find()
        .populate(["properties", "optiontypes", "taxonomies"]);
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  get(id) {
    try {
      return schema.protoType.findOne({ _id: id });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  create(req) {
    try {
      return schema.protoType
        .findOne({
          name: req.body.name,
        })
        .then(async (protoType) => {
          if (protoType && protoType.name) {
            throw new Error("prototype already exist");
          } else {
            const data = new schema.protoType(req.body);
            return await data.save();
          }
        });
    } catch (err) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  delete(id) {
    try {
      return schema.protoType.deleteOne({ _id: id });
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }

  edit(id, data) {
    try {
      return schema.protoType.findOneAndUpdate(
        { _id: id },
        { $set: data },
        { upsert: false, new: true }
      );
    } catch (error) {
      throw new Error("The resource you were looking for could not be found");
    }
  }
};
