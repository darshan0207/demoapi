const http = require("http");
var jwt = require("jsonwebtoken");
const jwt_decode = require("jwt-decode");
const app = require("./app.js");
const database = require("./utils/db");
const httpserver = http.createServer(app);
require("dotenv").config();
const port = process.env.PORT || 5001;
database.connect();

var io = require("socket.io")(httpserver);
io.use(function (socket, next) {
  // console.log(socket);
  // if (socket.handshake.query.token) {
  //   jwt.verify(
  //     socket.handshake.query.token,
  //     process.env.SECRET,
  //     function (err, decoded) {
  //       if (err) {
  //         console.log("failed 1", err);
  //         socket.send("authorization failed");
  //         socket.emit("disconnect", {});
  //         return next(new Error("Authentication error"));
  //       } else {
  //         // Subscribe to channels
  //         // let channels = decoded.email
  //         // eventEmitter.emit('socketId', {
  //         //   _id: decoded._id,
  //         //   socketId: socket.id
  //         // })
  //         console.log(decoded._id);
  //         socket.join(decoded._id, () => {
  //           next();
  //         });
  //         // socket.in(decoded._id).join(decoded._id)
  //         // socket.in(decoded._id).join("worldchat")
  //         socket.send("authorization successful");
  next();
  //       }
  //     }
  //   );
  // } else {
  //   socket.emit("disconnect", () => {
  //     console.log("disconnected");
  //   });
  //   console.log("failed 2");
  //   next(new Error("Authentication error"));
  // }
  // socket.on("disconnect", function (data) {
  //   console.log("disconnected ", socket.id);
  //   // socketService.disconnect(socket.id)
  // });
  // socket.on("check", (arg, callback) => {
  //   var decoded = jwt_decode(socket.handshake.query.token);
  //   callback({ check: "checking.." });
  // });
});

httpserver.listen(port, () => {
  console.log("HTTP Server running on port " + port);
});

const getSocketIo = () => io;
console.log(getSocketIo);
module.exports.getSocketIo = getSocketIo;
