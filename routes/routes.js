const express = require("express");
const router = express.Router();
const multer = require("multer");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./images");
  },
  filename: function (req, file, cb) {
    let extmimetype = file.mimetype.split("/");
    var extension;
    if (extmimetype.length) {
      extension = extmimetype[extmimetype.length - 1];
    }
    const uniqueSuffix = Date.now() + "-" + Math.round(Math.random() * 1e9);
    cb(null, file.fieldname + "-" + uniqueSuffix + "." + extension);
  },
});

const upload = multer({ storage: storage });

const optiontypecontroller = require("../api/optionType/controller");
const optionvaluecontroller = require("../api/optionValue/controller");
const propertietypecontroller = require("../api/propertie/controller");
const taxonomietypecontroller = require("../api/taxonomie/controller");
const prototypecontroller = require("../api/protoType/controller");
const productcontroller = require("../api/product/controller");
const stocklocationcontroller = require("../api/stock/location/controller");
const cartcontroller = require("../api/cart/controller");
const addresscontroller = require("../api/address/controller");
const taxcategorycontroller = require("../api/taxCategory/controller");
const paymentmethodcontroller = require("../api/paymentMethod/controller");
const countrycontroller = require("../api/country/controller");
const statecontroller = require("../api/state/controller");
const zonecontroller = require("../api/zones/controller");
const taxratecontroller = require("../api/taxrate/controller");
const homecontroller = require("../api/home/controller");
const ordercontroller = require("../api/order/controller");
const wishlistcontroller = require("../api/wishlist/controller");
const shippingcategorycontroller = require("../api/shippingCategory/controller");
const shippingmethodcontroller = require("../api/shippingMethod/controller");
const fileuploadcontroller = require("../api/fileUpload/service");
const authcontroller = require("../api/auth/controller");
const accountcontroller = require("../api/account/controller");
const bannercontroller = require("../api/banner/controller");
const taxoncontroller = require("../api/taxon/controller");
const currencycontroller = require("../api/currency/controller");
const categoriecontroller = require("../api/categorie/controller");
const subcategoriecontroller = require("../api/subcategorie/controller");
const deliveryinformationcontroller = require("../api/deliveryInformation/controller");
const pickupaddresscontroller = require("../api/pickupAddress/controller");
const deliveryUserController = require("../api/deliveryStaff/user/controller");
const restaurantUserController = require("../api/restaurantStaff/user/controller");

const authGuard = require("../guard/authenticate");

router.post(
  "/uploadfile",
  upload.single("files"),
  fileuploadcontroller.userLogin
);

router.use("/auth", authcontroller);
router.use("/user", authGuard, accountcontroller);
router.use("/optiontype", optiontypecontroller);
router.use("/optionvalue", optionvaluecontroller);
router.use("/propertietype", propertietypecontroller);
router.use("/taxonomietype", taxonomietypecontroller);
router.use("/prototype", prototypecontroller);
router.use("/product", productcontroller);
router.use("/stocklocation", stocklocationcontroller);
router.use("/cart", cartcontroller);
// router.use("/cart", authGuard, cartcontroller);
router.use("/address", authGuard, addresscontroller);
router.use("/taxcategory", taxcategorycontroller);
router.use("/paymentmethod", paymentmethodcontroller);
router.use("/country", countrycontroller);
router.use("/state", statecontroller);
router.use("/zone", zonecontroller);
router.use("/taxrate", taxratecontroller);
router.use("/wishlist", authGuard, wishlistcontroller);
router.use("/home", homecontroller);
router.use("/order", authGuard, ordercontroller);
router.use("/shippingcategory", shippingcategorycontroller);
router.use("/shippingmethod", shippingmethodcontroller);
router.use("/banner", bannercontroller);
router.use("/taxon", taxoncontroller);
// router.use("/categories", categoriescontroller);
router.use("/currency", currencycontroller);
router.use("/categorie", categoriecontroller);
router.use("/subcategorie", subcategoriecontroller);
router.use("/delivery-information", deliveryinformationcontroller);
router.use("/pickup", pickupaddresscontroller);

// delivery Staff
router.use("/delivery", deliveryUserController);

// restaurant Staff
router.use("/restaurant", restaurantUserController);

module.exports = router;
